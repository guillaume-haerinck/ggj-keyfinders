// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Delegates/IDelegateInstance.h"
#include "DefaultGameMode.generated.h"

/**
 * 
 */
UCLASS()
class KEYFINDERS_API ADefaultGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	UFUNCTION(BlueprintImplementableEvent, Category = "Input", Meta = (DisplayName = "On Controller Connection Change"))
	void OnControllerConnectionChange(bool Connected, int32 UserID, int32 ControllerID);

private:
	FDelegateHandle OnControllerConnectionHandle;
};
