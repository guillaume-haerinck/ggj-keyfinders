// Fill out your copyright notice in the Description page of Project Settings.


#include "DefaultGameMode.h"

void ADefaultGameMode::BeginPlay() 
{
	Super::BeginPlay();
	OnControllerConnectionHandle = FCoreDelegates::OnControllerConnectionChange.AddUFunction(this, FName("OnControllerConnectionChange"));
}

void ADefaultGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	FCoreDelegates::OnControllerConnectionChange.Remove(OnControllerConnectionHandle);
}
